<?php
/*
Plugin Name: Events filterer
Description: Basic WordPress plugin to filter throught post
Version:     1.0
Author:      Brahim BOUKOUFALLAH
Author URI:  https://brahoum.net
*/

/*
 * Set up Plugin Globals
 */
if ( ! class_exists( 'EventsFilterer' ) ) {
    final class EventsFilterer
    {
        /**
         * Register hooks.
         */
        public function register()
        {
            // Add query vars
            add_filter( 'query_vars' , array( $this , 'addQueryVars' ) );

            // Filter post title & date if it is set
            add_filter( 'pre_get_posts', array( $this , 'filterQueryPostDate' ) );
            add_filter( 'posts_where', array( $this , 'filterQueryPostTitleContent' ) , 10 , 2 );

            // Add action support for widgets
            add_action( 'eventsfilterer', array( $this , 'displayFilterForm' ) );
        }

        /**
         * Display filter form.
         *
         * @return string
         */
        public function displayFilterForm()
        {
            echo '
                <form method="POST">
                    <label for="date_event">Date de début</label>
                    <input type="date" id="date_event" name="date_event" />
                    <label for="date_fin">Date de début</label>
                    <input type="date" id="date_fin" name="date_fin" />
                    <br />
                    <label for="mot_cle">Mots clé</label>
                    <input type="text" id="mot_cle" name="mots_cle" />
                    <br />
                    <label for="price_number">Prix</label>
                    <input type="number" id="price_number" name="price_number" />
                    <input type="submit" value="Filtrer les événements">
                </form>
            ';
        }

        /**
         * add query vars.
         *
         * @param array $qvars
         */
        public function addQueryVars( $qvars )
        {
            $qvars[] = 'date_event';
            $qvars[] = 'date_fin';
            $qvars[] = 'mots_cle';
            $qvars[] = 'price_number';

            return $qvars;
        }

        /**
         * Filter query post title & content.
         *
         * @param string $where
         * @param WP_Query $wp_query
         *
         * @return string
         */
        public function filterQueryPostTitleContent( $where, &$wp_query )
        {
            global $wpdb;

            if( ( $wp_query->is_main_query() ) && ( !is_admin() ) ) {
                if ( $wp_query->get( 'mots_cle' )  && ! empty( $wp_query->query['mots_cle'] ) ) {
                    $where .= ' AND (' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( $wpdb->esc_like( $wp_query->get( 'mots_cle' ) ) ) . '%\'';
                    $where .= ' OR ' . $wpdb->posts . '.post_content LIKE \'%' . esc_sql( $wpdb->esc_like( $wp_query->get( 'mots_cle' ) ) ) . '%\')';
                }
            }

            return $where;
        }

        /**
         * Filter query post date
         *
         * @param WP_Query $wp_query
         *
         * @return WP_Query
         */
        public function filterQueryPostDate($query)
        {
            global $wp_query;

            $metaquery = array();

            if( ( $query->is_main_query() ) && ( !is_admin() ) ) {
                if ( isset( $wp_query->query['date_event'] ) && ! empty( $wp_query->query['date_event'] ) ) {
                    $startDate = strtotime($wp_query->query['date_event']);
                    $endDate = strtotime($wp_query->query['date_fin']);
                    $metaquery['details_date_debut_clause'] = array(
                        array(
                            'post_type' => 'sd-event',
                            'key'     => 'details_date_debut',
                            'value'   => array(date('Ymd', $startDate), date('Ymd', $endDate)),
                            'compare' => 'BETWEEN',
                            'type'          => 'DATE'
                        ),
                    );
                }

                if ( isset( $wp_query->query['price_number'] ) && ! empty( $wp_query->query['price_number'] ) ) {
                    $metaquery['details_prix_clause'] = array(
                        'post_type' => 'sd-event',
                        'key'     => 'details_prix',
                        'value'   => $wp_query->query['price_number'],
                        'compare' => '<=',
                        'type'    => 'NUMERIC'
                    );
                }
            }

            $wp_query->set('meta_query', $metaquery);

            return $query;
        }
    }
}

if ( class_exists( 'EventsFilterer' ) ) {
    global $eventsFilterer;
    $eventsFilterer = new EventsFilterer();

    $eventsFilterer->register();
}
?>
