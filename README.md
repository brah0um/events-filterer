Events filterer plugin
======================

Ceci est la correction du plugin que vous deviez développer.

Pré-requis
----------

Installer le plugin [Advanced Custom Fields](https://fr.wordpress.org/plugins/advanced-custom-fields/)

Créer un Custom Post Type (sd-event). Copier/Coller le code suivant dans le `functions.php` de votre thème:

```php
<?php
add_action ('init', 'register_sd_event_type');

function register_sd_event_type()
{
    register_post_type(
        'sd-event',
        array(
            'label' => 'Evenements',
            'labels' => array(
                'name' => 'Evenements',
                'singular_name' => 'Evenement',
                'all_items' => 'Tous les evenements',
                'add_new_item' => 'Ajouter un evenement',
                'edit_item' => 'Éditer l\'evenement',
                'new_item' => 'Nouvel evenement',
                'view_item' => 'Voir l\'evenement',
                'search_items' => 'Rechercher parmi les evenements',
                'not_found' => 'Pas d\'evenement trouvé',
                'not_found_in_trash'=> 'Pas d\'evenements dans la corbeille'
            ),
            'public' => true,
            'capability_type' => 'post',
            'supports' => array(
                'title',
                'editor',
                'thumbnail'
            ),
            'has_archive' => true
        )
    );
}
?>
```

Ajouter les champs ACF suivants pour les post_type égal à `sd-event`:
| Nom du champs      |   Type   |
| ------------------ | -------- |
| details_date_debut |   Date   |
|  details_date_fin  |   Date   |
|    details_prix    |  Number  |


Utilisation
-----------

Activer le plugin via l'interface d'administration.

Aller dans le fichier dans lequel vous souhaitez afficher le formulaire du plugin et ajoute rle code suivant:

```php
do_action( 'eventsfilterer' );
```
